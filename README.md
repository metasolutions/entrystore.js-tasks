# EntryStore.js Tasks

The purpose of this repository is to provide guidance on how to use EntryStore.js.

## Getting started

You need to have a working EntryStore installation and a web server that serves this 
repository from the same domain. You also need to provide a config file in the config directory (start from the config/config.js_example).

Before you can do anything you need to have npm and bower installed, then run:

    bower install
    npm install

To build the project (needed for the web application) you need to:

    cd build
    ./build.sh

## Running the web application

The web application allows you to add, view and remove tasks by going to the index.html.
Running the non-built version can be achieved by adding ?debug=true.


## Running node scripts

Simply run the scripts in bin

    cd bin
    ./ls