require.config({
    baseUrl: "./libs", //Path relative to bootstrapping html file.
    //Paths relative baseUrl, only those that deviate from baseUrl/{modulename}
    // are explicitly listed.
    paths: {
        "estasks": "..",
        "bootstrap": "bootstrap-amd/lib",
        "jquery": "jquery/src",
        "sizzle": "sizzle/dist/sizzle",
        "requireLib": "requirejs/require",
        "md5": "md5/js/md5.min"
    },
    packages: [ //Config defined using packages to allow for main.js when requiring just config.
        {
            name: "config",
            location: "../config",
            main: "config"
        }
    ],
    map: {
        "*": {
            "jquery": "jquery/jquery",  //In general, use the main module (for all unqualified jquery dependencies).
            "jquery/selector": "jquery/selector-sizzle", //Always use the jquery sizzle selector engine.
            "has": "dojo/has" //Use dojos has module since it is more clever.
        },
        "jquery": {
            "jquery": "jquery", //Reset (override general mapping) to normal path (jquerys has dependencies to specific modules).
            "jquery/selector": "jquery/selector-sizzle", //Always use the jquery sizzle selector engine.
            "external/sizzle/dist/sizzle": "sizzle"
        },
        "bootstrap": {
            "jquery": "jquery", //Reset (override general mapping) to normal path (bootstraps has dependencies to specific dependencies).
            "jquery/selector": "jquery/selector-sizzle" //Always use the jquery sizzle selector engine.
        },
        "store/rest": {
            "dojo/request": "dojo/request/xhr", //Force using xhr since we know we are in the browser
            "dojo/request/iframe": "dojo/request/iframe" //Override above line for iframe path.
        }
    },
    deps: [
        "estasks/tasks/app"
    ]
});