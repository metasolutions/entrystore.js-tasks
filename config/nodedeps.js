var cmd = process.argv[2];
var requirejs = require("requirejs");

requirejs.config({
    baseUrl: "../libs",
    //Paths relative baseUrl, only those that deviate from baseUrl/{modulename}
    // need to be explicitly listed.
    paths: {
        "estasks": "..",
        "md5": "md5/js/md5.min",
        "config": "../config/config"
    },
    deps: ["estasks/cmd/dojofix", "estasks/cmd/"+cmd]
});