define([
    "jquery",
    "store/EntryStore",
    "config",
    "rdfjson/namespaces"
], function(jquery, EntryStore, config, namespaces) {
    var es = new EntryStore();
    var context = es.getContextById(config.context);
    namespaces.add("ical", "http://www.w3.org/2002/12/cal/ical#");

    jquery(document).ready(function() {
        es.getAuth().login(config.user, config.password).then(function() {
            var tasks = jquery("#tasks");

            var renderTask = function(entry) {
                var title = entry.getMetadata().findFirstValue(null, "dcterms:title");
                var tr = jquery("<tr>").appendTo(tasks);
                jquery("<td>").text(title).appendTo(tr);
                var td = jquery("<td>").addClass("taskremove").appendTo(tr);
                jquery("<button type='button' class='btn btn-default'" +
                    " aria-label='Left Align'><span class='glyphicon glyphicon-remove'" +
                    " aria-hidden='true'></span></button>").appendTo(td).click(function() {
                    entry.del().then(renderTasks);
                });
                return tr;
            };

            var renderTasks = function() {
                tasks.html("");
                es.newSolrQuery().rdfType("ical:Vtodo").context(context).list().forEach(renderTask);
            };

            var addTask = function() {
                var desc = jquery("#input").val();
                var protEnt = es.getContextById(config.context).newEntry();
                var md = protEnt.getMetadata();
                var subj = protEnt.getResourceURI();
                md.add(subj, "rdf:type", "ical:Vtodo");
                md.addL(subj, "dcterms:title", desc);
                protEnt.commit().then(renderTask);
            };


            jquery("#addTask").click(addTask);
            renderTasks();
        });
    });
});

/*More possible fields for Vtodo:
"Summary":			dcterms:title //ical:summary
"Description":		dcterms:description //ical:description
"Comment":			ical:comment  rdfs:comment
"Status":			ical:status
"Priority":			ical:priority
"Completed":		ical:completed  //date
"Due":				ical:due //date
"PercentComplete":	ical:percentComplete
"Definition":		dcterms:subject
"URL":				ical:url // rdfs:seeAlso
"Created":			ical:created //dcterms:created date
"LastModified":		ical:lastModified // dcterms:modified date*/