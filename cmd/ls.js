define([
    "store/EntryStore",
    "config",
    "rdfjson/namespaces"
], function(EntryStore, config, namespaces) {
    var es = new EntryStore(config.base);
    var context = es.getContextById(config.context);
    namespaces.add("ical", "http://www.w3.org/2002/12/cal/ical#");

    es.getAuth().login(config.user, config.password).then(function() {
        es.newSolrQuery().rdfType("ical:Vtodo").context(context).list().forEach(function(entry) {
            var title = entry.getMetadata().findFirstValue(null, "dcterms:title");
            console.log("Task: "+title);
        });
    });
});